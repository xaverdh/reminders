#!/usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (ps: with ps; [ optparse-applicative haskeline ])"

-- import System.Process
import System.Directory (listDirectory)
import System.Console.Haskeline

import qualified System.IO as IO
import qualified Data.Map as M
import qualified Data.List as L
import Control.Monad.Trans.Maybe
import Control.Monad
import Control.Applicative
import Data.Maybe

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,flag',strOption,auto,option,info,metavar,showDefault)
import qualified Options.Applicative.Builder as OAB
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

startsWith :: Eq a => [a] -> [a] -> Bool
startsWith prefix = isJust . L.stripPrefix prefix

main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo program mempty
    prefs = OAB.defaultPrefs
      { prefDisambiguate = True }

hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)

data Output = Stdout | OutPath String

newtype Domain = Domain String
newtype Username = Username String
newtype Password = Password String

data Config = Config
  { configInterface :: Maybe String
  , configDomain :: Maybe String
  , configCertPath :: Maybe String
  , configUsername :: Maybe String
  , configPassword :: Maybe String }

emptyConfig = Config Nothing Nothing Nothing Nothing Nothing 


program :: Parser (IO ())
program = pure eduroamSetup
  <*> ( flag' Stdout
        ( long "stdout"
        <> help "Output the generated configuration to stdout instead of writing it to a file." )
      <|> ( fmap OutPath . strOption )
        ( long "out" <> short 'o'
        <> metavar "PATH"
        <> value "/var/lib/iwd/eduroam.8021x" <> showDefault
        <> help "Path to write the generated configuration to." ) )
  <*> ( pure Config <*> ifaceP <*> domainP
       <*> certPathP <*> unameP <*> passP )
  where
    domainP = ( optional . strOption )
      ( long "domain" <> short 'd'
      <> metavar "DOMAIN"
      <> help "The domain of your home institution." )
    certPathP = ( optional . strOption )
      ( long "certpath" <> short 'c'
      <> metavar "PATH"
      <> help "Path to server side certificate." )
    ifaceP = ( optional . strOption )
      ( long "iface" <> short 'i'
      <> metavar "INTERFACE"
      <> help "Wifi interface name." )
    unameP = ( optional . strOption )
      ( long "username" <> short 'u'
      <> metavar "USERNAME"
      <> help "Username at your institution." )
    passP = ( optional . strOption )
      ( long "password" <> short 'p'
      <> metavar "PASSWORD"
      <> help "Password at your institution." )


eduroamSetup :: Output -> Config -> IO ()
eduroamSetup output config = do
  ifaces <- filter isWlan <$> listDirectory "/sys/class/net"
  mbSnippet <- runInputT defaultSettings . runMaybeT
    $ assemble config (listToMaybe ifaces)
  case mbSnippet of
    Nothing -> IO.hPutStrLn IO.stderr "something went wrong.."
    Just snippet -> case output of
      Stdout -> IO.putStr snippet
      OutPath path -> IO.writeFile path snippet
  where
    isWlan = startsWith "wl"

assemble :: (Monad m,MonadException m)
  => Config -> Maybe String -> MaybeT (InputT m) String
assemble config mbiface = do
  domain <- knowOrAsk configDomain askDomain 
  capath <- knowOrAsk configCertPath askCertPath
  iface <- knowOrAsk configInterface askInterface
  uname <- knowOrAsk configUsername askUsername
  pass <- knowOrAsk configPassword askPassword
  pure $ createSnippet
      (Domain $ "eduroam@" <> domain) capath
      (Username $ uname <> "@" <> domain)
      (Password pass)
  where
    knowOrAsk f ask = MaybeT $ maybe ask (pure . Just) (f config)
    askDomain = getInputLineWithInitial "domain: "
      ("uni-augsburg.de","")
    askCertPath = getInputLineWithInitial "path to certificate: "
      ("/etc/ssl/certs/ca-certificates.crt","")
    askInterface = getInputLineWithInitial "wifi interface name: "
      (fromMaybe "" mbiface ,"")
    askUsername = getInputLine "RZ username: "
    askPassword = getPassword (Just '*') "RZ password: "

createSnippet :: Domain -> String -> Username -> Password -> String
createSnippet (Domain d) capath (Username uname) (Password passwd) = mkIni $ M.fromList
  [ "Security" ~> M.fromList
    [ "EAP-Method" ~> "TTLS"
    , "EAP-TTLS-Phase2-Method" ~> "Tunneled-PAP"
    , "EAP-Identity" ~> d
    , "EAP-PEAP-CACert" ~> capath
    , "EAP-TTLS-Phase2-Identity" ~> uname
    , "EAP-TTLS-Phase2-Password" ~> passwd ]
  , "Settings" ~> M.fromList
    [ "Autoconnect" ~> "true" ] ]
  where a ~> b = (a,b)

mkIni :: M.Map String (M.Map String String) -> String
mkIni =
  let mkSection = M.foldMapWithKey (\k v -> k <> "=" <> v <> "\n")
   in M.foldMapWithKey (\k vs -> "[" <> k <> "]\n" <> mkSection vs )

